﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace _05DoesntHeHaveInternElvesForThis {
    class Program {

        static Regex vowels = new Regex(@"[aeiou]");
        static Regex repeated = new Regex(@"(\w)\1{1,}");
        static Regex disallowed = new Regex(@"(ab|cd|pq|xy)");

        static Regex repeatedNotOverlapped = new Regex(@".*(\w\w).*\1");
        static Regex repeatOneBetween = new Regex(@"(\w).{1}\1");

        static void Main(string[] args) {
            Console.WriteLine("Day 5 - Doesn't He Have Intern-Elves For This?");
            Console.ReadLine();
            FileInfo inputFile = new FileInfo("input.txt");
            FileStream fs = inputFile.OpenRead();
            StreamReader inputReader = new StreamReader(fs);
            List <string> naughty = new List<string>(),nice = new List<string>();
            while (!inputReader.EndOfStream) {
                String s = inputReader.ReadLine();
                if(TestFirst(s)) { nice.Add(s); }
                else { naughty.Add(s); }
            }
            Console.Write("First rules: " + nice.Count + " nice strings, " + naughty.Count + " naughty strings.");
            Console.ReadLine();


            fs.Position = 0; //Reverse, reverse!
            naughty.Clear();
            nice.Clear();
            while (!inputReader.EndOfStream) {
                String s = inputReader.ReadLine();
                if (TestSecond(s)) { nice.Add(s); }
                else { naughty.Add(s); }
            }
            Console.Write("Second rules: " + nice.Count + " nice strings, " + naughty.Count + " naughty strings.");

            Console.ReadLine();
        }

        static Boolean TestFirst(String s) {
            Boolean passVowels = TestVowels(s), passRepeated = TestRepeated(s), passDisallowed = TestDisallowed(s);
            Console.ForegroundColor = passVowels && passRepeated && passDisallowed ? ConsoleColor.Green : ConsoleColor.Red;
            Console.Write(s.PadRight(18) + "\t");
            Console.ForegroundColor = ConsoleColor.Gray;
            WriteBoolColored(passVowels);
            Console.Write("\t");
            WriteBoolColored(passRepeated);
            Console.Write("\t");
            WriteBoolColored(passDisallowed);
            Console.WriteLine();
            return passVowels && passRepeated && passDisallowed;
        }

        private static Boolean TestSecond(String s) {
            Boolean passRepeatedNotOverlapped = TestRepeatedNotOverlapped(s), passRepeatedOneBetween = TestRepeatedOneBetween(s);
            Console.ForegroundColor = passRepeatedNotOverlapped && passRepeatedOneBetween ? ConsoleColor.Green : ConsoleColor.Red;
            Console.Write(s.PadRight(18) + "\t");
            Console.ForegroundColor = ConsoleColor.Gray;
            WriteBoolColored(passRepeatedNotOverlapped);
            Console.Write("\t");
            WriteBoolColored(passRepeatedOneBetween);
            Console.WriteLine();
            return passRepeatedNotOverlapped && passRepeatedOneBetween;
        }

        static Boolean TestVowels(String s) {
            return vowels.Matches(s).Count >= 3;
        }

        static Boolean TestRepeated(String s) {
            return repeated.IsMatch(s);
        }

        static Boolean TestDisallowed(String s) {
            return !disallowed.IsMatch(s);
        }

        static Boolean TestRepeatedNotOverlapped(String s) {
            return repeatedNotOverlapped.IsMatch(s);
        }

        static Boolean TestRepeatedOneBetween(String s) {
            return repeatOneBetween.IsMatch(s);
        }

        static void WriteBoolColored(Boolean b) {
            ConsoleColor tempFG = Console.ForegroundColor;
            Console.ForegroundColor = b ? ConsoleColor.Green : ConsoleColor.Red;
            Console.Write(b);
            Console.ForegroundColor = tempFG;
        }
    }
}
