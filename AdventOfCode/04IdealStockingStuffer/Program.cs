﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Threading;
using System.IO;

namespace _04IdealStockingStuffer
{
    public class HashThread
    {
        private int answer = -1;
        public int getAnswer() { return answer; }

        private int i;
        public int getI() { return i; }

        public HashThread(int start) {
            this.i = start;
        }

        public HashThread() :this(0) {     
        }

        public void loop()
        {
            MD5 md5hash = MD5.Create();
            string SecretKey = "ckczppom", hashResult = "";
            do
            {
                i++;
                hashResult = GetMd5Hash(md5hash, SecretKey + i);
                Console.WriteLine(i + "\t" + hashResult);
            }
            //while (!hashResult.StartsWith("00-00-0"));//Part 1
            while (!hashResult.StartsWith("00-00-00"));//Part 2
            answer = i;
            Console.WriteLine(i);
            
        }

        static string GetMd5Hash(MD5 md5Hash, string input)//from https://msdn.microsoft.com/en-us/library/system.security.cryptography.md5(v=vs.110).aspx
        {
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            return BitConverter.ToString(data);           
        }
    }
    public class SaveThread {
        private HashThread ht;
        private FileStream fs;
        private bool running = true;

        public SaveThread(HashThread t, FileStream fs) {
            this.ht = t;
            this.fs = fs;
        }
        public void loop() {
            while (running) {
                fs.Position = 0;
                BinaryWriter bw = new BinaryWriter(fs);
                bw.Write((int)ht.getI());
                //Console.WriteLine("Written output!");
                Thread.Sleep(30000);
            }
        }

        public void stop() {
           this.running = false;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            int startIndex = 0;
            FileInfo saveFile = new FileInfo("save.dat");
            if (!saveFile.Exists) {
                saveFile.Create();
                Console.WriteLine("No save file found");
            }
            else {
                BinaryReader saveReader = new BinaryReader(saveFile.OpenRead());
                try {
                    startIndex = saveReader.ReadInt32();
                }
                catch (Exception e) {
                    Console.WriteLine("Error reading");
                    startIndex = 0;
                }
                finally {
                    Console.WriteLine("saveReader Closed");
                    saveReader.Close();
                }
            }
            //startIndex = 0;
            Console.WriteLine(startIndex);
            Console.ReadKey();

            FileStream fs = saveFile.OpenWrite();

            HashThread ht = new HashThread(startIndex);
            SaveThread st = new SaveThread(ht, fs);
            Thread Thash = new Thread(new ThreadStart(ht.loop));
            Thread Tsave = new Thread(new ThreadStart(st.loop));
            Thash.Start();
            Tsave.Start();

            ConsoleKey key;
            do {
                key = Console.ReadKey().Key;
            } while (key!=ConsoleKey.Enter);

            st.stop();

            fs.Close();
        }


        
    }
}
