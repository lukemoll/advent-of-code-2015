﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace _07SomeAssemblyRequired {
    class Program {

        public delegate ushort ValueCalculator();

        static Regex Sides = new Regex(@"(.*) -> (.+)");// \1: LHS \2: RHS

        static Regex ValueR = new Regex(@"^(\d+)$");// \1: Value
        static Regex DirectR = new Regex(@"^([a-z]+)$");// \1: arg
        static Regex AndOrR = new Regex(@"(\w+) (AND|OR) (\w+)");// \1: arg1 \2: operation \3: arg2
        static Regex ShiftR = new Regex(@"(\w+) (LSHIFT|RSHIFT) (\d+)");// \1: arg \2 operation \3: factor
        static Regex NotR = new Regex(@"NOT (\w+)");// \1: arg        

        static Dictionary<String, ValueCalculator> nodes = new Dictionary<string, ValueCalculator>();

        static List<string> ReadLines() {
            FileInfo inputFile = new FileInfo("input.txt");
            //Console.WriteLine(inputFile.Length);
            StreamReader inputReader = new StreamReader(inputFile.OpenRead());
            List<string> lines = new List<string>();
            while (!inputReader.EndOfStream) {
                lines.Add(inputReader.ReadLine());
            }
            return lines;
        }

        static void Main(string[] args) {
            //String input = "123 -> x\n456 -> y\nx AND y -> d\nx OR y -> e\nx LSHIFT 2 -> f\ny RSHIFT 2 -> g\nNOT x -> h\nNOT y -> i";
            //List<string> lines = new List<string>(input.Split('\n'));
            List<string> lines = ReadLines();
            Console.ReadLine();
            foreach(string line in lines) {
                //Console.WriteLine(line);
                ValueCalculator g;
                String lhs, rhs;
                GroupCollection groups = Sides.Match(line).Groups;
                lhs = groups[1].Value;
                rhs = groups[2].Value;
                g = GetCalculator(lhs);
                nodes.Add(rhs, g);
            }
            Console.ReadLine();
            ValueCalculator v;
            nodes.TryGetValue("a", out v);
            Console.WriteLine(v());

            /*(foreach(KeyValuePair<String,ValueCalculator> p in nodes) {
                Console.ReadLine();
                Console.Write(p.Key + ":\t");
                Console.WriteLine(p.Value());
            }*/
            Console.WriteLine("Done!");
            Console.ReadKey();
        }

        static ushort GetValue(String key) {
            Console.Write(key + " ");
            if(ValueR.IsMatch(key)) {
                return Convert.ToUInt16(key);
            }
            else {
                ValueCalculator v;
                if (!nodes.TryGetValue(key, out v)) {
                    Console.WriteLine(key + " not found");
                }

                return v();
            }
        }

        static ValueCalculator GetCalculator(String lhs) {
            Console.ForegroundColor = ConsoleColor.Red;
            //Console.Write(lhs);
            ValueCalculator g = () => { Console.WriteLine("ERROR"); return 0; };
            if (ValueR.IsMatch(lhs)) {
                //Console.WriteLine(" is value");
                ushort value = Convert.ToUInt16(ValueR.Match(lhs).Groups[1].Value);
                g = () => value;
            } else if (DirectR.IsMatch(lhs)) {
                Console.WriteLine(lhs + " is direct connection " + lhs.Length);
                String s = DirectR.Match(lhs).Groups[1].Value;
                Console.WriteLine("Group 1: " + s);
                g = () => { return GetValue(s); }; /**/
            } else if (AndOrR.IsMatch(lhs)) {
                //Console.WriteLine(" is AND/OR");
                ushort arg1, arg2;
                GroupCollection groups = AndOrR.Match(lhs).Groups;
                bool isAnd = groups[2].Value.Equals("AND");
                if (isAnd) {
                    g = () => And(GetValue(groups[1].Value), GetValue(groups[3].Value));
                }
                else {
                    g = () => Or(GetValue(groups[1].Value), GetValue(groups[3].Value));
                }
            } else if (ShiftR.IsMatch(lhs)) {
                //Console.WriteLine(" is SHIFT");
                ushort arg, factor;
                GroupCollection groups = ShiftR.Match(lhs).Groups;
                factor = Convert.ToUInt16(groups[3].Value);
                bool isLeft = groups[2].Value.Equals("LSHIFT");
                if (isLeft) {
                    g = () => LeftShift(GetValue(groups[1].Value), factor);
                }
                else {
                    g = () => RightShift(GetValue(groups[1].Value), factor);
                }
            } else if (NotR.IsMatch(lhs)) {
                //Console.WriteLine(" is NOT");
                g = () => Not(GetValue(NotR.Match(lhs).Groups[1].Value));
            }
            else {
                Console.WriteLine(lhs + " not recognised");
            }

            Console.ForegroundColor = ConsoleColor.Gray;
            return g;
        }

        static ushort Not(ushort arg) {
            return (ushort)(~arg);
        }

        static ushort And(ushort arg1, ushort arg2) {
            return (ushort)(arg1 & arg2);
        }

        static ushort Or(ushort arg1, ushort arg2) {
            return (ushort)(arg1 | arg2);
        }

        static ushort LeftShift(ushort arg, ushort factor) {
            return (ushort)(arg << factor);
        }

        static ushort RightShift(ushort arg, ushort factor) {
            return (ushort)(arg >> factor);
        }

        static void PrintUShort(ushort n) {
            Console.Write(Convert.ToString(n, 2).PadLeft(16, '0'));
        }
    }
}

