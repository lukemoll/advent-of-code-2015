﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02ToldNoMath
{
    class Program
    {
        static void Main(string[] args)
        {

            FileInfo inputFile = new FileInfo("input.txt");
            //Console.WriteLine(inputFile.Length);
            StreamReader inputReader = new StreamReader(inputFile.OpenRead());
            List<string> lines = new List<string>();
            while (!inputReader.EndOfStream)
            {
                lines.Add(inputReader.ReadLine());
            }

            int totalArea = 0, totalRibbon = 0;
            foreach (String line in lines)
            {
                int l, w, h;
                totalRibbon += GetRibbonRequired(ParseLine(line, out l, out w, out h));
                totalArea += GetAreaRequired(l, w, h);
            }
            Console.WriteLine("The elves need " + totalArea + " square feet of wrapping paper.");
            Console.WriteLine("The elves need " + totalRibbon + " feet of ribbon.");
            Console.ReadLine();

        }


        static int[] ParseLine(string line, out int l, out int w, out int h)
        {
            String[] dims = line.Split(new char[] { 'x' });
            l = Convert.ToInt16(dims[0]);
            w = Convert.ToInt16(dims[1]);
            h = Convert.ToInt16(dims[2]);
            List<int> result = new List<int>();
            result.Add(l); result.Add(w); result.Add(h);
            result.Sort();
            return result.ToArray();
        }

        static int GetAreaRequired(int l, int w, int h)
        {
            int area = 0;//2*l*w + 2*w*h + 2*h*l + area of smallest side
            area += 2 * l * w;
            area += 2 * w * h;
            area += 2 * h * l;
            //side can be l*w, l*h, or h*w
            area += Math.Min(l * w, Math.Min(l * h, h * w));
            return area;
        }

        static int GetRibbonRequired(int[] dims)
        {
            int ribbon = 0;
            int x = dims[0], y = dims[1];//x&y are lowest pair of values in dims
            ribbon += 2 * x;
            ribbon += 2 * y;
            ribbon += dims[0] * dims[1] * dims[2];
            return ribbon;

        }

    }
}
