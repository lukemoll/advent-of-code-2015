﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;

namespace _06ProbablyFireHazard {
    class Program {

        public struct UIntPoint {//Slight modification from day 03
            public uint x, y;
        }

        static bool[,] fireHazard = new bool[1000,1000];//Oooh, fancy non-jagged 2D array
        static short[,] secondHazard = new short[1000, 1000];
        static Regex lineParser = new Regex(@"(turn (off|on)|toggle) ([0-9]+),([0-9]+) through ([0-9]+),([0-9]+)");//Actually beautiful

        static void Main(string[] args) {
            Console.WriteLine("Day 6 - Probably A Fire Hazard");
            Turn(new UIntPoint { x = 0,y=0 },new UIntPoint{ x = 999,y = 999},false);

            Console.WriteLine(CountSecond());
            FileInfo inputFile = new FileInfo("input.txt");
            FileStream fs = inputFile.OpenRead();
            StreamReader inputReader = new StreamReader(fs);
            
            while (!inputReader.EndOfStream) {
                String s = inputReader.ReadLine();
                ParseLine(s);
            }

            Bitmap b = new Bitmap(1000, 1000);//http://stackoverflow.com/a/4585082/3612949
            for (int x=0;x<=999;x++) {
                for(int y=0;y<=999;y++) {
                    b.SetPixel(x, y, fireHazard[x, y]?Color.White:Color.Black);
                }
            }
            b.Save(@"output.png", ImageFormat.Png);

            Console.WriteLine(CountFirst());
            Console.WriteLine(CountSecond());
            Console.ReadKey();
        }

        static void ParseLine(String line) {
            Match m = lineParser.Match(line);
            UIntPoint start, end;
            start.x = Convert.ToUInt16(m.Groups[3].Value);
            start.y = Convert.ToUInt16(m.Groups[4].Value);
            end.x = Convert.ToUInt16(m.Groups[5].Value);
            end.y = Convert.ToUInt16(m.Groups[6].Value);
            if (m.Groups[1].Value.StartsWith("turn"))
            {
                Turn(start, end, m.Groups[1].Value.Contains("on"));
            }
            else
            {
                Toggle(start, end);
            }
            //Console.WriteLine(CountSecond());
            //Console.ReadKey();
        }

        static void Turn(UIntPoint start, UIntPoint end, bool value) {
            for(uint x = start.x; x <= end.x; x++) {
                for(uint y = start.y; y<= end.y; y++) {
                    fireHazard[x, y] = value;
                    secondHazard[x, y] += (short)(value ? 1 : -1);
                    if(secondHazard[x,y]<0) { secondHazard[x,y]= 0; }
                }
            }
        }

        static void Toggle(UIntPoint start, UIntPoint end) {
            for (uint x = start.x; x <= end.x; x++) {
                for (uint y = start.y; y <= end.y; y++) {
                    fireHazard[x, y] = !fireHazard[x,y];
                    secondHazard[x, y] += 2;
                }
            }
        }

        static int CountFirst() {
            int count = 0;
            foreach(bool b in fireHazard) {
                count += b ? 1 : 0;
            }
            return count;
        }

        static int CountSecond() {
            int count = 0;
            foreach (byte b in secondHazard) {
                count += b;
            }
            return count;
        }

    }
}
